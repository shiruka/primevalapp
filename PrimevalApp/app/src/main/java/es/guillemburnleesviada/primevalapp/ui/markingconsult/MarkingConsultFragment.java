package es.guillemburnleesviada.primevalapp.ui.markingconsult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.databinding.FragmentMarkingConsultBinding;

public class MarkingConsultFragment extends Fragment implements ViewBinding {

    private MarkingConsultViewModel viewModel;
    private FragmentMarkingConsultBinding binding;
    private List<Marking> markingList;

    public static MarkingConsultFragment newInstance() {
        return new MarkingConsultFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentMarkingConsultBinding.inflate(inflater, container, false);

        markingList = new ArrayList<>();

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MarkingConsultViewModel.class);

        viewModel.doGetMarkingList();

        Observer<ArrayList<Marking>> observerMarking = markings -> {
            if (markings != null) {
                markingList.clear();
                markingList.addAll(markings);
                setMarkings();
            }
        };

        viewModel.getMarkinglist().observe(getViewLifecycleOwner(), observerMarking);

        binding.spMarkingName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                String select = adapter1.getItem(position);

                Marking selectedMark = new Marking();

                for (int i = 0; i < markingList.size(); i++) {
                    if (markingList.get(i).getMarkname().equals(select)) {
                        selectedMark = markingList.get(i);
                        break;
                    }
                }

                binding.txtAtribute.setText(selectedMark.getAttribute());
                binding.txtEvent.setText(selectedMark.getEvent());
                binding.txtProbability.setText(selectedMark.getChance() + "");
                binding.txtType.setText(selectedMark.getType());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setMarkings() {

        ArrayList<String> spinnerList = new ArrayList<>();

        for (int i = 0; i < markingList.size(); i++) {
            if (!markingList.get(i).isStrong())
                spinnerList.add(markingList.get(i).getMarkname());
        }

        String[] data = spinnerList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, data);
        binding.spMarkingName.setAdapter(adapter);

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
