package es.guillemburnleesviada.primevalapp.ui.colorblender;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.api.DataWebService;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.utils.Constants;

public class ColorBlenderViewModel extends ViewModel {

    private MutableLiveData<ArrayList<StarterDino>> dinoList = new MutableLiveData<>();

    public MutableLiveData<ArrayList<StarterDino>> getDinoList() {
        if (dinoList == null) {
            dinoList = new MutableLiveData<>();
        }
        return dinoList;
    }

    public void doGetDinoList() {
        new DataWebService().getListDinos((code, object) -> {
            if (code == Constants.SERVER_SUCCESS_CODE) {
                String jsonString = object.toString();
                Type listType = new TypeToken<List<StarterDino>>() {
                }.getType();

                ArrayList<StarterDino> temp = new Gson().fromJson(jsonString, listType);

                for (int i = 0; i < temp.size(); i++) {
                    if (temp.get(i).getFeathered() == 0) {
                        temp.get(i).setFeathered(false);
                    } else {
                        temp.get(i).setFeathered(true);
                    }
                }

                dinoList.setValue(temp);

                for (int i = 0; i < dinoList.getValue().size(); i++) {
                    ArrayList<Marking> markings = new ArrayList<>();
                    dinoList.getValue().get(i).setMarkings(markings);
                }

            } else {
                Log.d("FAILURE", "error failed to call dinos");
            }
        });
    }

}
