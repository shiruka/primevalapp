package es.guillemburnleesviada.primevalapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StarterDino implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("species")
    private String species;
    @SerializedName("sex")
    private String sex;
    @SerializedName("color")
    private String color;
    private ArrayList<Marking> markings;
    @SerializedName("isFeathered")
    private int Feathered;

    private boolean isFeatheredBool;

    public StarterDino() {
    }

    public StarterDino(int id, String name, String species, String sex, String color, int Feathered) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.sex = sex;
        this.color = color;
        this.Feathered = Feathered;
    }

    public StarterDino(int id, String name, String species, String sex, String color, ArrayList<Marking> markings, int Feathered) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.sex = sex;
        this.color = color;
        this.markings = markings;
        this.Feathered = Feathered;
    }

    public StarterDino(int id, String name, String species, String sex, String color, ArrayList<Marking> markings, int Feathered, boolean isFeatheredBool) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.sex = sex;
        this.color = color;
        this.markings = markings;
        this.Feathered = Feathered;
        this.isFeatheredBool = isFeatheredBool;
    }

    protected StarterDino(Parcel in) {
        id = in.readInt();
        name = in.readString();
        species = in.readString();
        sex = in.readString();
        color = in.readString();
        Feathered = in.readInt();
        isFeatheredBool = in.readByte() != 0;
    }

    public static final Creator<StarterDino> CREATOR = new Creator<StarterDino>() {
        @Override
        public StarterDino createFromParcel(Parcel in) {
            return new StarterDino(in);
        }

        @Override
        public StarterDino[] newArray(int size) {
            return new StarterDino[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ArrayList<Marking> getMarkings() {
        return markings;
    }

    public void setMarkings(ArrayList<Marking> markings) {
        this.markings = markings;
    }

    public int getFeathered() {
        return Feathered;
    }

    public void setFeatheredBool(int featheredBool) {
        this.Feathered = featheredBool;
    }

    public boolean isFeatheredBool() {
        return isFeatheredBool;
    }

    public void setFeathered(boolean feathered) {
        isFeatheredBool = feathered;
    }

    @Override
    public String toString() {
        return "StarterDino{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", species='" + species + '\'' +
                ", sex='" + sex + '\'' +
                ", color='" + color + '\'' +
                ", markings=" + markings +
                ", isFeathered=" + Feathered +
                ", isFeatheredBool=" + isFeatheredBool +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(species);
        dest.writeString(sex);
        dest.writeString(color);
        dest.writeInt(Feathered);
        dest.writeByte((byte) (isFeatheredBool ? 1 : 0));
    }
}
