package es.guillemburnleesviada.primevalapp.ui.colorblender;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.guillemburnleesviada.primevalapp.R;
import es.guillemburnleesviada.primevalapp.adapters.AdapterColor;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.databinding.FragmentColorBlenderBinding;
import es.guillemburnleesviada.primevalapp.utils.GenBlend;
import es.guillemburnleesviada.primevalapp.utils.RGBClass;

public class ColorBlenderFragment extends Fragment implements ViewBinding {

    private ColorBlenderViewModel viewModel;
    private FragmentColorBlenderBinding binding;
    private List<StarterDino> dinoArrayList;
    private ArrayList<RGBClass> classArrayList;
    private String selectedMom;
    private String selectedDad;

    public static ColorBlenderFragment newInstance() {
        return new ColorBlenderFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentColorBlenderBinding.inflate(inflater, container, false);

        dinoArrayList = new ArrayList<>();

        classArrayList = new ArrayList<>();


        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(ColorBlenderViewModel.class);

        AdapterColor adapterColor = new AdapterColor(classArrayList, getContext(), R.layout.dino_card);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);

        binding.recyclerColors.setAdapter(adapterColor);
        binding.recyclerColors.setLayoutManager(layoutManager);
        binding.recyclerColors.setHasFixedSize(true);

        viewModel.doGetDinoList();
        Observer<ArrayList<StarterDino>> observerDino = starterDinos -> {
            if (starterDinos != null) {
                dinoArrayList.clear();
                dinoArrayList.addAll(starterDinos);

                List<String> tempmom = new ArrayList<>();
                List<String> tempdad = new ArrayList<>();

                for (int i = 0; i < dinoArrayList.size(); i++) {
                    if (dinoArrayList.get(i).getSex().equals("female")) {
                        tempmom.add(dinoArrayList.get(i).getName());
                    } else if (dinoArrayList.get(i).getSex().equals("male")) {
                        tempdad.add(dinoArrayList.get(i).getName());
                    }
                }

                String[] dataMom = tempmom.toArray(new String[0]);
                ArrayAdapter<String> adapterMom = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, dataMom);
                binding.spMother.setAdapter(adapterMom);

                String[] dataDad = tempdad.toArray(new String[0]);
                ArrayAdapter<String> adapterDad = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, dataDad);
                binding.spFather.setAdapter(adapterDad);

            }
        };

        viewModel.getDinoList().observe(getViewLifecycleOwner(), observerDino);

        binding.spMother.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                String select = adapter1.getItem(position);

                selectedMom = select;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spFather.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                String select = adapter1.getItem(position);

                selectedDad = select;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StarterDino mom = new StarterDino();
                StarterDino dad = new StarterDino();

                for (int i = 0; i < dinoArrayList.size(); i++) {
                    if (dinoArrayList.get(i).getName().equals(selectedMom)) {
                        mom = dinoArrayList.get(i);
                    } else if (dinoArrayList.get(i).getName().equals(selectedDad)) {
                        dad = dinoArrayList.get(i);
                    }
                }

                RGBClass[] colorlist = GenBlend.ReturnBlendedColors(new RGBClass(mom.getColor()), new RGBClass(dad.getColor()));

                classArrayList.clear();

                classArrayList.addAll(Arrays.asList(colorlist));

                adapterColor.notifyDataSetChanged();

            }
        });

        binding.btnRandomColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RGBClass[] colorlist = GenBlend.ReturnBlendedColors(RGBClass.RandomRGB(), RGBClass.RandomRGB());

                classArrayList.clear();

                classArrayList.addAll(Arrays.asList(colorlist));

                adapterColor.notifyDataSetChanged();

            }
        });

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
