package es.guillemburnleesviada.primevalapp.api;

public class Config {
    /**
     * My api server. Root url for the DataStrategy
     */
    public static final String API_URL = "http://ns3036637.ip-164-132-201.eu:3000";
    public static final String HTTP_CLIENT_AUTHORIZATION = "Bearer ";
    public static final String TYPE_ITEM_AUTHORIZATION = "Authorization";
    public static final String TYPE_ITEM_WS_AUTH = "ws_auth";
    public static final String GRANT_TYPE_LOGIN = "password";
    public static final String GRANT_TYPE = "refresh_token";
    public static final String CLIENT_ID = "";
    public static final String CLIENT_SECRET = "";
}
