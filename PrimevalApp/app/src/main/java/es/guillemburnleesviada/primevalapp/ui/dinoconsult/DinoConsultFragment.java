package es.guillemburnleesviada.primevalapp.ui.dinoconsult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.data.models.Gendino;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.databinding.FragmentDinoConsultBinding;
import es.guillemburnleesviada.primevalapp.utils.Utils;

public class DinoConsultFragment extends Fragment implements ViewBinding {

    private DinoConsultViewModel viewModel;
    private FragmentDinoConsultBinding binding;

    private List<StarterDino> dinoArrayList;
    private List<Marking> markingList;
    private List<Gendino> gendinoList;
    private List<String> speciesList;
    private String lastDino;
    private String lastSpecies;

    public static DinoConsultFragment newInstance() {
        return new DinoConsultFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentDinoConsultBinding.inflate(inflater, container, false);

        dinoArrayList = new ArrayList<>();
        markingList = new ArrayList<>();
        gendinoList = new ArrayList<>();

        lastDino = "";
        lastSpecies = "";

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(DinoConsultViewModel.class);
        viewModel.doGetDinoList();
        viewModel.doGetMarkingList();
        viewModel.doGetGendinoList();

        Observer<ArrayList<StarterDino>> observerDino = starterDinos -> {
            if (starterDinos != null) {
                dinoArrayList.clear();
                dinoArrayList.addAll(starterDinos);
                getSpeciesList();

            }
        };

        Observer<ArrayList<Marking>> observerMarking = markings -> {
            if (markings != null) {
                markingList.clear();
                markingList.addAll(markings);
            }
        };

        Observer<ArrayList<Gendino>> observerGendino = gendinos -> {
            if (gendinos != null) {
                gendinoList.clear();
                gendinoList.addAll(gendinos);
                getGendino();
            }
        };

        viewModel.getDinoList().observe(getViewLifecycleOwner(), observerDino);
        viewModel.getMarkinglist().observe(getViewLifecycleOwner(), observerMarking);
        viewModel.getGendinoList().observe(getViewLifecycleOwner(), observerGendino);

        binding.spSpecies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                String select = adapter1.getItem(position);

                lastSpecies = select;

                List<String> tempdino = new ArrayList<>();

                for (int i = 0; i < dinoArrayList.size(); i++) {
                    if (dinoArrayList.get(i).getSpecies().equals(select)) {
                        tempdino.add(dinoArrayList.get(i).getName());
                    }
                }

                String[] dataMom = tempdino.toArray(new String[0]);
                ArrayAdapter<String> adapterMom = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, dataMom);
                binding.spDino.setAdapter(adapterMom);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spDino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                lastDino = adapter1.getItem(position);

                StarterDino dino = Utils.ReturnParentFromList(dinoArrayList, lastDino);

                binding.txtDinoId.setText("ID: " + dino.getId());
                binding.txtColors.setText(dino.getColor());
                binding.txtDinoSex.setText(dino.getSex());
                if (dino.isFeatheredBool()) {
                    binding.txtIsFeathered.setText("Feathered");
                } else {
                    binding.txtIsFeathered.setText("Bald");
                }

                String marks = "";

                for (int i = 0; i < dino.getMarkings().size(); i++) {

                    marks += dino.getMarkings().get(i).getAttribute() + " / ";

                }

                binding.txtGenoShow.setText(marks);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getSpeciesList() {

        speciesList = new ArrayList<>();

        for (int i = 0; i < dinoArrayList.size(); i++) {
            if (!speciesList.contains(dinoArrayList.get(i).getSpecies())) {
                speciesList.add(dinoArrayList.get(i).getSpecies());
            }
        }

        String[] data = speciesList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, data);
        binding.spSpecies.setAdapter(adapter);

    }

    private void getGendino() {

        ArrayList<Marking> dinoGen;

        for (int i = 0; i < dinoArrayList.size(); i++) {
            dinoGen = new ArrayList<>();
            for (int j = 0; j < gendinoList.size(); j++) {
                if (gendinoList.get(j).getId_dino() == dinoArrayList.get(i).getId()) {
                    for (int k = 0; k < markingList.size(); k++) {
                        if (gendinoList.get(j).getId_marking() == markingList.get(k).getId()) {
                            dinoGen.add(markingList.get(k));
                        }
                    }
                }
            }
            dinoArrayList.get(i).setMarkings(dinoGen);
        }

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
