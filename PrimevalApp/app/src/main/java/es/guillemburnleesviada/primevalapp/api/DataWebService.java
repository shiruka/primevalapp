package es.guillemburnleesviada.primevalapp.api;

import android.util.Log;

import com.google.gson.JsonArray;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import es.guillemburnleesviada.primevalapp.BuildConfig;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataWebService extends DataStrategy {

    private Retrofit retrofit;
    private ApiService apiService;
    private String TAG_FAILURE = "FAILURE_CALL";

    /**
     * Constructor to create a retrofit client to make calls to api.
     */
    public DataWebService() {
        OkHttpClient clientWithoutAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientWithoutAuth)
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    /**
     * Overrides DataStrategy functions
     * @param interactDispatcherObject
     */

    @Override
    public void getListDinos(InteractDispatcherObject interactDispatcherObject) {

        apiService.getListDinos().enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d(TAG_FAILURE, "onFailure: get dinos: " + t.getMessage());
            }
        });
    }

    @Override
    public void getListMarkings(InteractDispatcherObject interactDispatcherObject) {

        apiService.getListMarkings().enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d(TAG_FAILURE, "onFailure: get markings: " + t.getMessage());
            }
        });

    }

    @Override
    public void getListGendinos(InteractDispatcherObject interactDispatcherObject) {
        apiService.getListGendinos().enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d(TAG_FAILURE, "onFailure: get gendinos: " + t.getMessage());
            }
        });
    }

}
