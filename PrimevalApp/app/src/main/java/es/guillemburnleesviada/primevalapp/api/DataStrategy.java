package es.guillemburnleesviada.primevalapp.api;

import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.GET;

public abstract class DataStrategy {

    //Lists
    /**
     * Calls the api and gets a json with a list of StarterDino objects
     *
     * @param interactDispatcherObject
     */
    public abstract void getListDinos(InteractDispatcherObject interactDispatcherObject);

    /**
     * Calls the api and gets a json with a list of Marking objects
     *
     * @param interactDispatcherObject
     */
    public abstract void getListMarkings(InteractDispatcherObject interactDispatcherObject);

    /**
     * Calls the api and gets a json with a list of Gendino objects.
     * Gendino is used to relationate a marking with a dino.
     *
     * @param interactDispatcherObject
     */
    public abstract void getListGendinos(InteractDispatcherObject interactDispatcherObject);

    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface ApiService {

        @GET("/api/dinos")
        Call<JsonArray> getListDinos();

        @GET("/api/dinos/listmarkings")
        Call<JsonArray> getListMarkings();

        @GET("/api/dinos/liststarters")
        Call<JsonArray> getListStarters();

        @GET("/api/dinos/listgendino")
        Call<JsonArray> getListGendinos();

    }

}
