package es.guillemburnleesviada.primevalapp.ui.incubatorresult;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewbinding.ViewBinding;

import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.databinding.FragmentIncubatorResultBinding;
import es.guillemburnleesviada.primevalapp.utils.RGBClass;

public class IncubatorResultFragment extends Fragment implements ViewBinding {

    private IncubatorResultViewModel mViewModel;
    private FragmentIncubatorResultBinding binding;
    private StarterDino egg;
    private String mom;
    private String dad;


    public IncubatorResultFragment(StarterDino egg, String mom, String dad) {
        this.egg = egg;
        this.mom = mom;
        this.dad = dad;
    }

    public static IncubatorResultFragment newInstance(StarterDino egg, String mom, String dad) {
        return new IncubatorResultFragment(egg, mom, dad);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentIncubatorResultBinding.inflate(inflater, container, false);

        binding.txtMotherName.setText(mom);
        binding.txtFatherName.setText(dad);

        binding.txtSpeciesName.setText(egg.getSpecies());
        binding.txtSex.setText(egg.getSex());
        binding.txtColorResult.setText(egg.getColor());
        String temp = "";
        for (int i = 0; i < egg.getMarkings().size(); i++) {
            temp += egg.getMarkings().get(i).getAttribute() + " / ";
        }

        if (egg.isFeatheredBool()) {
            temp += "\nFeathered";
        } else {
            temp += "\nBald";
        }

        binding.txtGenos.setText(temp);

        RGBClass rgb = new RGBClass(egg.getColor());

        ColorDrawable cd = new ColorDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            cd.setColor(Color.valueOf(Color.rgb(rgb.getR(), rgb.getG(), rgb.getB())).toArgb());
            binding.imgColorDinoResult.setImageDrawable(cd);
        } else {
            Toast.makeText(getContext(), "Cannot show rgb color correctly, android version too old", Toast.LENGTH_SHORT).show();
        }

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(IncubatorResultViewModel.class);

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
