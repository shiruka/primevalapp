package es.guillemburnleesviada.primevalapp.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import es.guillemburnleesviada.primevalapp.data.models.StarterDino;

public class GenBlend {

    public static int[] ReturnVectorSteps(int start, int stop) {
        List<Integer> steps = new ArrayList<>();

        int step = 12;

        IntStream.range(0, step);

        IntStream temp = IntStream.range(0, step);

        for (int i : temp.toArray()) {
            steps.add((int) Math.round(start + (stop - start) * ((double) i / (step - 1))));
        }

        if (steps.get(steps.size() - 1) != stop) {
            steps.set(steps.size() - 1, stop);
        }

        return convertListToArray(steps);
    }

    private static int[] convertListToArray(List<Integer> listResult) {
        int[] result = new int[listResult.size()];
        int i = 0;
        for (int num : listResult) {
            result[i++] = num;
        }
        return result;
    }

    public static RGBClass[] ReturnBlendedColors(RGBClass mom, RGBClass dad) {
        RGBClass[] lista = new RGBClass[12];

        int[] R = ReturnVectorSteps(mom.getR(), dad.getR());
        int[] G = ReturnVectorSteps(mom.getG(), dad.getG());
        int[] B = ReturnVectorSteps(mom.getB(), dad.getB());

        for (int i = 0; i < 12; i++) {
            lista[i] = new RGBClass(R[i], G[i], B[i]);
        }
        return lista;
    }

    public static String ReturnRandomBlend(StarterDino mom, StarterDino dad) {
        RGBClass[] lista = ReturnBlendedColors(new RGBClass(mom.getColor()), new RGBClass(dad.getColor()));
        RGBClass result = lista[NumericUtils.GenRoll(0, 11)];

        return result.toString();
    }

    public static RGBClass ReturnRandomBlendRGBClass(StarterDino mom, StarterDino dad) {
        RGBClass[] lista = ReturnBlendedColors(new RGBClass(mom.getColor()), new RGBClass(dad.getColor()));
        RGBClass result = lista[NumericUtils.GenRoll(0, 11)];

        return result;
    }

    public static RGBClass ReturnRandomBlend(RGBClass mom, RGBClass dad) {
        RGBClass[] lista = ReturnBlendedColors(mom, dad);
        RGBClass result = lista[NumericUtils.GenRoll(0, 11)];

        return result;
    }
}
