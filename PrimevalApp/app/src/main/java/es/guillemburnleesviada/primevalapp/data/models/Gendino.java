package es.guillemburnleesviada.primevalapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Gendino {

    @SerializedName("id")
    private int id;
    @SerializedName("id_dino")
    private int id_dino;
    @SerializedName("id_marking")
    private int id_marking;

    public Gendino() {
    }

    public Gendino(int id, int id_dino, int id_marking) {
        this.id = id;
        this.id_dino = id_dino;
        this.id_marking = id_marking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_dino() {
        return id_dino;
    }

    public void setId_dino(int id_dino) {
        this.id_dino = id_dino;
    }

    public int getId_marking() {
        return id_marking;
    }

    public void setId_marking(int id_marking) {
        this.id_marking = id_marking;
    }

    @Override
    public String toString() {
        return "Gendino{" +
                "id=" + id +
                ", id_dino=" + id_dino +
                ", id_marking=" + id_marking +
                '}';
    }
}
