package es.guillemburnleesviada.primevalapp.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;

public class Utils {

    public static RGBClass lastRGB;

    public static String GenSex() {

        int roll = NumericUtils.GenRoll(1, 100);

        if (roll <= 50) {
            return "male";
        } else {
            return "female";
        }

    }

    public static StarterDino ReturnParentFromList(List<StarterDino> StarterDinoList, String name) {
        int posDinoList = 0;
        //Get parents position in Main list
        for (int i = 0; i < StarterDinoList.size(); i++) {
            if (StarterDinoList.get(i).getName().equals(name)) posDinoList = i;
        }
        return StarterDinoList.get(posDinoList);
    }

    public static StarterDino EggConstructor(StarterDino mother, StarterDino father, List<Marking> markingList) {

        StarterDino egg = new StarterDino();//init

        egg.setFeathered(RollIsFeathered(mother.isFeatheredBool(), father.isFeatheredBool()));//roll if it has feathers

        if (mother.getSpecies().equals(father.getSpecies())) { //Should always be true
            egg.setSpecies(mother.getSpecies());//set species
        }

        egg.setSex(GenSex());//roll sex

        lastRGB = GenBlend.ReturnRandomBlendRGBClass(mother, father);//generate color

        egg.setColor(lastRGB.toString());

        egg.setMarkings(MarkingsIncubatorConstructorNew(mother, father));//generate egg markings

        return egg;

    }

    private static boolean RollIsFeathered(boolean mother, boolean father) {
        if (mother && father) {
            return true;
        } else if (mother || father) {
            int roll = NumericUtils.GenRoll(1, 100);

            if (roll <= 50) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static ArrayList<Marking> MarkingsIncubatorConstructorNew(StarterDino mother, StarterDino father) {

        ArrayList<Marking> temp = new ArrayList<>();
        ArrayList<Marking> temp2 = new ArrayList<>();

        temp.addAll(mother.getMarkings());
        temp.addAll(father.getMarkings());

        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i).isStrong()) {
                for (int j = 0; j < 2; j++) {
                    if (NumericUtils.GenRoll(1, 100) <= temp.get(i).getChance()) {
                        temp2.add(temp.get(i));
                    }
                }
            } else {
                if (NumericUtils.GenRoll(1, 100) <= temp.get(i).getChance()) {
                    temp2.add(temp.get(i));
                }
            }
        }

        for (int i = 0; i < temp2.size(); i++) {
            if ((Collections.frequency(temp2, temp2.get(i))) > 1) {
                temp2.remove(temp2.get(i));
            }
        }

        return temp2;

    }

}