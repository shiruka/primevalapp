package es.guillemburnleesviada.primevalapp.ui.incubator;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.api.DataWebService;
import es.guillemburnleesviada.primevalapp.data.models.Gendino;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.utils.Constants;

public class IncubatorViewModel extends ViewModel {

    private MutableLiveData<ArrayList<StarterDino>> dinoList = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Marking>> markinglist = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Gendino>> gendinolist = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Marking>> getMarkinglist() {
        if (markinglist == null) {
            markinglist = new MutableLiveData<>();
        }
        return markinglist;
    }

    public MutableLiveData<ArrayList<StarterDino>> getDinoList() {
        if (dinoList == null) {
            dinoList = new MutableLiveData<>();
        }
        return dinoList;
    }

    public MutableLiveData<ArrayList<Gendino>> getGendinoList() {
        if (gendinolist == null) {
            gendinolist = new MutableLiveData<>();
        }
        return gendinolist;
    }

    public void doGetDinoList() {
        new DataWebService().getListDinos((code, object) -> {
            if (code == Constants.SERVER_SUCCESS_CODE) {
                String jsonString = object.toString();
                Type listType = new TypeToken<List<StarterDino>>() {
                }.getType();

                ArrayList<StarterDino> temp = new Gson().fromJson(jsonString, listType);

                for (int i = 0; i < temp.size(); i++) {
                    if (temp.get(i).getFeathered() == 0) {
                        temp.get(i).setFeathered(false);
                    } else {
                        temp.get(i).setFeathered(true);
                    }
                }

                dinoList.setValue(temp);

                for (int i = 0; i < dinoList.getValue().size(); i++) {
                    ArrayList<Marking> markings = new ArrayList<>();
                    dinoList.getValue().get(i).setMarkings(markings);
                }

                Log.i("onResponseDino", dinoList.getValue().toString());

            } else {
                Log.d("FAILURE", "error failed to call dinos");
            }
        });
    }

    public void doGetMarkingList() {
        new DataWebService().getListMarkings((code, object) -> {
            if (code == Constants.SERVER_SUCCESS_CODE) {
                String jsonString = object.toString();
                Type listType = new TypeToken<List<Marking>>() {
                }.getType();

                ArrayList<Marking> temp = new Gson().fromJson(jsonString, listType);

                for (int i = 0; i < temp.size(); i++) {
                    if (temp.get(i).getStrength() == 0) {
                        temp.get(i).setStrong(false);
                    } else {
                        temp.get(i).setStrong(true);
                    }
                }

                markinglist.setValue(temp);

                Log.i("onResponseMarking", markinglist.getValue().toString());
            }
        });
    }

    public void doGetGendinoList() {
        new DataWebService().getListGendinos((code, object) -> {
            if (code == Constants.SERVER_SUCCESS_CODE) {
                String jsonString = object.toString();
                Type listType = new TypeToken<List<Gendino>>() {
                }.getType();
                gendinolist.setValue(new Gson().fromJson(jsonString, listType));

                Log.i("onResponseGendino", gendinolist.getValue().toString());
            }
        });
    }

}
