package es.guillemburnleesviada.primevalapp.ui.markingconsult;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.api.DataWebService;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.utils.Constants;

public class MarkingConsultViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Marking>> markinglist = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Marking>> getMarkinglist() {
        if (markinglist == null) {
            markinglist = new MutableLiveData<>();
        }
        return markinglist;
    }

    public void doGetMarkingList() {
        new DataWebService().getListMarkings((code, object) -> {
            if (code == Constants.SERVER_SUCCESS_CODE) {
                String jsonString = object.toString();
                Type listType = new TypeToken<List<Marking>>() {
                }.getType();

                ArrayList<Marking> temp = new Gson().fromJson(jsonString, listType);

                for (int i = 0; i < temp.size(); i++) {
                    if (temp.get(i).getStrength() == 0) {
                        temp.get(i).setStrong(false);
                    } else {
                        temp.get(i).setStrong(true);
                    }
                }

                markinglist.setValue(temp);

                Log.i("onResponseMarking", markinglist.getValue().toString());
            }
        });
    }

}
