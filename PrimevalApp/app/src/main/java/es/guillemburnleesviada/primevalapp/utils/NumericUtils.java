package es.guillemburnleesviada.primevalapp.utils;

import java.util.concurrent.ThreadLocalRandom;

public class NumericUtils {

    public static int GenRoll(int minimumValue, int maximumValue) {

        if (minimumValue < maximumValue) {
            return ThreadLocalRandom.current().nextInt(minimumValue, maximumValue + 1);
        } else {
            return -1;
        }
    }

}
