package es.guillemburnleesviada.primevalapp.ui.rng;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import es.guillemburnleesviada.primevalapp.databinding.FragmentRandomNumberGenBinding;
import es.guillemburnleesviada.primevalapp.utils.NumericUtils;

public class RandomNumberGenFragment extends Fragment {

    private FragmentRandomNumberGenBinding binding;

    private RandomNumberGenViewModel mViewModel;

    private int max;
    private int min;

    public static RandomNumberGenFragment newInstance() {
        return new RandomNumberGenFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentRandomNumberGenBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RandomNumberGenViewModel.class);
        max = 0;
        min = 0;

        binding.txtMaxLayout.setError("Max can't be equal or smaller to Min");
        binding.txtMinLayout.setError("Min can't be equal or bigger to Max");

        initTextWatchers();
        doRandomNumber();
    }

    private void initTextWatchers() {
        binding.txtMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    if (Integer.parseInt(s.toString()) <= min) {
                        setInputErrorState(true);
                    } else {
                        setInputErrorState(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.txtMin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    if (Integer.parseInt(s.toString()) >= max) {
                        setInputErrorState(true);
                    } else {
                        setInputErrorState(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doRandomNumber() {
        binding.btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(binding.txtMin.getText().toString()) && !TextUtils.isEmpty(binding.txtMax.getText().toString())) {

                    min = Integer.parseInt(binding.txtMin.getText().toString());
                    max = Integer.parseInt(binding.txtMax.getText().toString());

                    if (checkFields()) {
                        binding.txtResult.setText(NumericUtils.GenRoll(min, max) + "");
                    } else {
                        Toast.makeText(getActivity(), "Check inputs", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Inputs are empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean checkFields() {

        return (max > min);

    }

    private void setInputErrorState(boolean b) {
        binding.txtMinLayout.setErrorEnabled(b);
        binding.txtMaxLayout.setErrorEnabled(b);
    }

}
