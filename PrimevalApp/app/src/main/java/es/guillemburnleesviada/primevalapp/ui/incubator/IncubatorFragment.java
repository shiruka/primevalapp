package es.guillemburnleesviada.primevalapp.ui.incubator;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

import es.guillemburnleesviada.primevalapp.R;
import es.guillemburnleesviada.primevalapp.data.models.Gendino;
import es.guillemburnleesviada.primevalapp.data.models.Marking;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.databinding.FragmentIncubatorBinding;
import es.guillemburnleesviada.primevalapp.ui.incubatorresult.IncubatorResultFragment;
import es.guillemburnleesviada.primevalapp.utils.Utils;

public class IncubatorFragment extends Fragment implements ViewBinding {

    private IncubatorViewModel viewModel;
    private FragmentIncubatorBinding binding;
    private List<String> speciesList;
    private List<Marking> markingList;
    private List<StarterDino> dinoArrayList;
    private List<Gendino> gendinoList;
    private String lastMom;
    private String lastDad;
    private String lastSpecies;

    public static IncubatorFragment newInstance() {
        return new IncubatorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentIncubatorBinding.inflate(inflater, container, false);

        dinoArrayList = new ArrayList<>();
        markingList = new ArrayList<>();
        gendinoList = new ArrayList<>();

        lastDad = "";
        lastMom = "";
        lastSpecies = "";

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(IncubatorViewModel.class);
        viewModel.doGetDinoList();
        viewModel.doGetMarkingList();
        viewModel.doGetGendinoList();

        Observer<ArrayList<StarterDino>> observerDino = starterDinos -> {
            if (starterDinos != null) {
                dinoArrayList.clear();
                dinoArrayList.addAll(starterDinos);
                getSpeciesList();

            }
        };

        Observer<ArrayList<Marking>> observerMarking = markings -> {
            if (markings != null) {
                markingList.clear();
                markingList.addAll(markings);
            }
        };

        Observer<ArrayList<Gendino>> observerGendino = gendinos -> {
            if (gendinos != null) {
                gendinoList.clear();
                gendinoList.addAll(gendinos);
                getGendino();
            }
        };

        viewModel.getDinoList().observe(getViewLifecycleOwner(), observerDino);
        viewModel.getMarkinglist().observe(getViewLifecycleOwner(), observerMarking);
        viewModel.getGendinoList().observe(getViewLifecycleOwner(), observerGendino);

        binding.spSpecies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                String select = adapter1.getItem(position);

                lastSpecies = select;

                List<String> tempmom = new ArrayList<>();
                List<String> tempdad = new ArrayList<>();

                for (int i = 0; i < dinoArrayList.size(); i++) {
                    if (dinoArrayList.get(i).getSex().equals("female") && dinoArrayList.get(i).getSpecies().equals(select)) {
                        tempmom.add(dinoArrayList.get(i).getName());
                    } else if (dinoArrayList.get(i).getSex().equals("male") && dinoArrayList.get(i).getSpecies().equals(select)) {
                        tempdad.add(dinoArrayList.get(i).getName());
                    }
                }

                String[] dataMom = tempmom.toArray(new String[0]);
                ArrayAdapter<String> adapterMom = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, dataMom);
                binding.spMother.setAdapter(adapterMom);

                String[] dataDad = tempdad.toArray(new String[0]);
                ArrayAdapter<String> adapterDad = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, dataDad);
                binding.spFather.setAdapter(adapterDad);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spMother.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                lastMom = adapter1.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spFather.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter1 = (ArrayAdapter<String>) parent.getAdapter();
                lastDad = adapter1.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StarterDino egg = Utils.EggConstructor(Utils.ReturnParentFromList(dinoArrayList, lastMom), Utils.ReturnParentFromList(dinoArrayList, lastDad), markingList);

                Log.d("EGG", egg.toString());

                IncubatorResultFragment show = new IncubatorResultFragment(egg, lastMom, lastDad);

                getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, show, "nav_incubator").addToBackStack(null).commit();

            }
        });

    }

    private void getSpeciesList() {

        speciesList = new ArrayList<String>();

        for (int i = 0; i < dinoArrayList.size(); i++) {
            if (!speciesList.contains(dinoArrayList.get(i).getSpecies())) {
                speciesList.add(dinoArrayList.get(i).getSpecies());
            }
        }

        String[] data = speciesList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, data);
        binding.spSpecies.setAdapter(adapter);

    }

    private void getGendino() {

        ArrayList<Marking> dinoGen;

        for (int i = 0; i < dinoArrayList.size(); i++) {
            dinoGen = new ArrayList<>();
            for (int j = 0; j < gendinoList.size(); j++) {
                if (gendinoList.get(j).getId_dino() == dinoArrayList.get(i).getId()) {
                    for (int k = 0; k < markingList.size(); k++) {
                        if (gendinoList.get(j).getId_marking() == markingList.get(k).getId()) {
                            dinoGen.add(markingList.get(k));
                        }
                    }
                }
            }
            dinoArrayList.get(i).setMarkings(dinoGen);
        }

    }

    @NonNull
    @Override
    public View getRoot() {
        return null;
    }
}
