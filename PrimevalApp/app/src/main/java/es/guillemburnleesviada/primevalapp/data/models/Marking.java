package es.guillemburnleesviada.primevalapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Marking {

    @SerializedName("id")
    private int id;
    @SerializedName("markname")
    private String markname;
    @SerializedName("attribute")
    private String attribute;
    @SerializedName("chance")
    private int chance;
    private boolean strong;
    @SerializedName("event")
    private String event;
    @SerializedName("type")
    private String type;
    @SerializedName("strength")
    private int strength;

    public Marking() {
    }

    public Marking(int id, String markname, String attribute, int chance, boolean strong, String event, String type) {
        this.id = id;
        this.markname = markname;
        this.attribute = attribute;
        this.chance = chance;
        this.strong = strong;
        this.event = event;
        this.type = type;
    }

    public Marking(int id, String markname, String attribute, int chance, boolean strong, String event, String type, int strength) {
        this.id = id;
        this.markname = markname;
        this.attribute = attribute;
        this.chance = chance;
        this.strong = strong;
        this.event = event;
        this.type = type;
        this.strength = strength;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarkname() {
        return markname;
    }

    public void setMarkname(String markname) {
        this.markname = markname;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public int getChance() {
        return chance;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public boolean isStrong() {
        return strong;
    }

    public void setStrong(boolean strong) {
        this.strong = strong;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "Marking{" +
                "id=" + id +
                ", markname='" + markname + '\'' +
                ", attribute='" + attribute + '\'' +
                ", chance=" + chance +
                ", strong=" + strong +
                ", event='" + event + '\'' +
                ", type='" + type + '\'' +
                ", strength=" + strength +
                '}';
    }
}
