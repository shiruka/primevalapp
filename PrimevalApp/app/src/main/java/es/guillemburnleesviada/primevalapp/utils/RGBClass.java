package es.guillemburnleesviada.primevalapp.utils;

import org.apache.commons.lang3.StringUtils;

public class RGBClass {

    private int r, g, b;

    public RGBClass(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public RGBClass(String RGB) { //"rgb(123, 123, 123)"
        String[] c = {"r", "g", "b", "(", ")"};

        for (int i = 0; i < c.length; i++) {
            RGB = StringUtils.replace(RGB, c[i], "");
        }

        RGB = RGB.trim();

        String[] temp = RGB.split(",");

        int[] pipo = new int[3];

        for (int i = 0; i < temp.length; i++) {
            pipo[i] = Integer.parseInt(temp[i]);
        }

        this.r = pipo[0];
        this.g = pipo[1];
        this.b = pipo[2];
    }

    @Override
    public String toString() {
        return "rgb(" + r + "," + g + "," + b + ")";
    }

    public static RGBClass RandomRGB() {
        int min = 0, max = 255;

        return new RGBClass(NumericUtils.GenRoll(min, max), NumericUtils.GenRoll(min, max), NumericUtils.GenRoll(min, max));
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }


}
