package es.guillemburnleesviada.primevalapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import es.guillemburnleesviada.primevalapp.R;
import es.guillemburnleesviada.primevalapp.data.models.StarterDino;
import es.guillemburnleesviada.primevalapp.utils.RGBClass;

public class AdapterDino extends RecyclerView.Adapter<AdapterDino.Card> {

    private ArrayList<StarterDino> dinoList;
    private Context context;
    private int resource;

    public AdapterDino(ArrayList<StarterDino> dinoList, Context context, int resource) {
        this.dinoList = dinoList;
        this.context = context;
        this.resource = resource;
    }


    @NonNull
    @Override
    public AdapterDino.Card onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cardFila = LayoutInflater.from(context).inflate(resource, null);
        Card card = new Card(cardFila);

        return card;
    }

    @Override
    public void onBindViewHolder(@NonNull Card holder, int position) {

        TextView name = holder.txtName;
        TextView species = holder.txtSpecies;
        TextView sex = holder.txtSex;
        ImageView color = holder.imgColor;

        final StarterDino d = dinoList.get(position);

        name.setText(d.getName());
        species.setText(d.getSpecies());
        sex.setText(d.getSex());

        ColorDrawable cd = new ColorDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            RGBClass rgb = new RGBClass(d.getColor());
            cd.setColor(Color.valueOf(Color.rgb(rgb.getR(), rgb.getG(), rgb.getB())).toArgb());
            color.setImageDrawable(cd);
        } else {
            Toast.makeText(context, "Missing API, android too old", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public int getItemCount() {
        return dinoList.size();
    }

    public class Card extends RecyclerView.ViewHolder {
        private ImageView imgColor;
        private TextView txtName, txtSpecies, txtSex;

        public Card(@NonNull View itemView) {
            super(itemView);

            imgColor = itemView.findViewById(R.id.imgColorDinoCard);
            txtName = itemView.findViewById(R.id.txtNameDinoCard);
        }
    }
}
