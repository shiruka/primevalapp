package es.guillemburnleesviada.primevalapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import es.guillemburnleesviada.primevalapp.R;
import es.guillemburnleesviada.primevalapp.utils.RGBClass;

public class AdapterColor extends RecyclerView.Adapter<AdapterColor.Card> {

    private ArrayList<RGBClass> rgbClasses;
    private Context context;
    private int resource;

    public AdapterColor(ArrayList<RGBClass> rgbClasses, Context context, int resource) {
        this.rgbClasses = rgbClasses;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public AdapterColor.Card onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cardFila = LayoutInflater.from(context).inflate(resource, null);
        Card card = new Card(cardFila);

        return card;
    }

    @Override
    public void onBindViewHolder(@NonNull Card holder, int position) {
        ImageView color = holder.imgColor;


        final RGBClass rgb = rgbClasses.get(position);

        holder.rgb.setText(rgb.toString());

        ColorDrawable cd = new ColorDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            cd.setColor(Color.valueOf(Color.rgb(rgb.getR(), rgb.getG(), rgb.getB())).toArgb()); //Sets imageView with a color from argb
            color.setImageDrawable(cd);
        } else {
            Toast.makeText(context, "Cannot show rgb color correctly, android version too old", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return rgbClasses.size();
    }

    public class Card extends RecyclerView.ViewHolder {
        private ImageView imgColor;
        private TextView rgb, title1, title2;

        public Card(@NonNull View itemView) {
            super(itemView);

            imgColor = itemView.findViewById(R.id.imgColorDinoCard);
            rgb = itemView.findViewById(R.id.txtNameDinoCard);
        }
    }
}
